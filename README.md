# README #

An Uppsala University beamer theme, offering a few more choices than the last theme did.

### Progress ###

* There are three layouts to choose from, based on the University's official graphics profile: etikett, marginal, and bard.
* An example file is included, showing some of the options.
* still in heavy development